export default class Place {

  constructor(id, short, label, position) {
    this._id = id;
    this._short = short;
    this._label = label;
    this._position = position;
    this._active = false;
  }

  get id() {
    return this._id;
  }

  get short() {
    return this._short;
  }

  get label() {
    return this._label;
  }

  get position() {
    return this._position;
  }

  get active() {
    return this._active;
  }

  set active(active) {
    this._active = active;
  }

  hits(position, tolerance) {
    const x = Math.abs(this._position.x - position.x);
    const y = Math.abs(this._position.y - position.y);
    return x < tolerance && y < tolerance;
  }
}