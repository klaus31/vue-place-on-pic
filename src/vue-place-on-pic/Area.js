export default class Area {
  /**
   * @param name used for identification
   * @param upperLeft Position
   * @param lowerRight Position
   */
  constructor(name, upperLeft, lowerRight) {
    this._name = name;
    this._upperLeft = upperLeft;
    this._lowerRight = lowerRight;
  }

  get name() {
    return this._name;
  }

  contains(position) {
    return this._upperLeft.x <= position.x &&
      this._upperLeft.y <= position.y &&
      this._lowerRight.x >= position.x &&
      this._lowerRight.y >= position.y;
  }

}