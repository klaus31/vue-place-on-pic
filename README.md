# vue-place-on-pic

Show a custom picture where you can mark points, react on clicking specific points and interactivly add new points.

## Usage

### Info on pic

```html
<template>
  <PlaceOnPic pic="pic.png" 
              :places="places" 
              @hit-place="doSomethingWith"></PlaceOnPic>
</template>

<script>
  import PlaceOnPic from "../vue-place-on-pic/PlaceOnPic";
  import Place from "../vue-place-on-pic/Place";
  import Position from "../vue-place-on-pic/Position";

  export default {
    components: {PlaceOnPic},
    data() {
      return {
        places: [
          new Place(1, 'A', 'Place on Pic 1', new Position(374, 132)),
          new Place(2, 'B', 'Place on Pic 2', new Position(443, 246)),
      }
    },
    methods: {
      doSomethingWith(place) {
        console.info(`${place.name} clicked`);
      }
    }
  }
</script>
```

### Possible parameters of `PlaceOnPic`

All parameters have a detailed description in the demo section.

|Parameter|Description|
|---|---|
|`pic` (required)|The image to show|
|`places` (required)|Array of places to show on image|
|`@hit-place`|Called when clicked on place (with `Place` as parameter)|
|`@click-map`|Called when clicked on map which is not a place or an area (with `Position` as parameter)|
|`areas`|Specific areas on the map|
|`@hit-area`|Called when an area clicked (with clicked `Area` as parameter)|
|`exit-button`|When present, an exit button ("close") is shown|
|`@exit`|Called, when Exit-Button clicked|
|`place-style`|Rudimental parameters to style marked places|
